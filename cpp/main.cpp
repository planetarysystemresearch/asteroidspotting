#define JSON_HAS_CPP_14
#define JSON_HAS_CPP_11

#include <dirent.h>
#include "include/fdeep/fdeep.hpp"

// Size of the input images
const uint16_t origW = 1024;
const uint16_t origH = 1024;

/// Struct to hold the results of the analysis
struct Mixed {
    std::string name;
    int score;
	float confidence;
};

/// Function to transform image to tensor
fdeep::tensor img_to_tensor(std::vector <uint16_t>& imageData, int img_width, int img_height){
	
    // Vector of floats to store the (maybe resized) image. Notice it's a 1D vector.
    std::vector<float> pixels;

	uint16_t stepW = origW / img_width;
	uint16_t stepH = origH / img_height;

	float new_pixel;
  	uint16_t out_pixel;

	int resize_ratio = origW / img_width; // Assuming square images
	//std::cout << resize_ratio << std::endl;

	// Resizing
    for (int h = 0; h < img_height; h++) {
        for (int w = 0; w < img_width; w++) {

			if (resize_ratio == 1) {

				// No resizing required
				new_pixel = imageData.at( w + h*origW );

			} else if (resize_ratio == 2) {

				// Resizing by half (that is, averaging by groups of two by two pixels) 
				new_pixel = imageData.at( (w * stepW)  	  + (h * stepH) 	* origW ) +
							imageData.at( (w * stepW + 1) + (h * stepH) 	* origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW + 1) + (h * stepH + 1) * origW );

				new_pixel = new_pixel / (resize_ratio * resize_ratio);

			} else if (resize_ratio == 4) {

				// Resizing by a quarter (averaging in groups of four by four)
				new_pixel = imageData.at( (w * stepW)     + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH)		* origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 1) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 2) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 3) * origW );

				new_pixel = new_pixel / (resize_ratio * resize_ratio);

			} else if (resize_ratio == 8) {

				// Resizing by an eighth (averaging groups of eight by eight)
				new_pixel = imageData.at( (w * stepW)     + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH)		* origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH)		* origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 1) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 1) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 2) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 2) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 3) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 3) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 4) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 4) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 5) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 5) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 6) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 6) * origW ) +

							imageData.at( (w * stepW)     + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 1 + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 2 + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 3 + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 4 + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 5 + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 6 + (h * stepH + 7) * origW ) +
							imageData.at( (w * stepW) + 7 + (h * stepH + 7) * origW );

				new_pixel = new_pixel / (resize_ratio * resize_ratio);

			} else {
				std::cout << "Incompatible size." << std::endl;
			}

			pixels.push_back(new_pixel);

        }

    }

	//std::cout << "Resized" << std::endl;
	
	/* Store the image for debug purposes */

	/*
	static uint32_t cnt = 0;
	cnt++;
	std::string fName = std::to_string(cnt) + "_" + std::to_string(img_height) + "_" + std::to_string(img_width);
	std::ofstream f (fName, std::ios::binary);

	std::cout << "Writing " << pixels.size() << " skipped output pixels" << std::endl;

    for(std::vector<float>::size_type i = 0; i < pixels.size(); i++) {
	    out_pixel = pixels[i];
		f.write((const char*)&out_pixel, sizeof(uint16_t));
		}

	f.close();
	*/

	const fdeep::tensor t(fdeep::tensor_shape(img_width, img_height, 1), pixels);

	return t;

}

/// Function to load image and return a tensor.
fdeep::tensor load_img(std::string img_path, int img_width, int img_height) {

    // Stream for image file
	std::ifstream f (img_path, std::ios::binary);
	
	// Vector to store the image in (it's a 1D vector!)
	std::vector<uint16_t> imageData16bit;

	if (f.good()) {

		for (uint32_t i = 0; i < origW * origH; i++) {

		    // These must be the three channels, and it's keeping only the last, or b, depending on the size of c?
			uint16_t a;
			uint8_t b;
			uint8_t c;

			f.read((char *)&b, 1);	// Reads one character from the stream and stores it in b
			f.read((char *)&c, 1);	// Stores the same character? Or the next? (Not the same)

			a = ((uint16_t)c << 8 ) | b;

			imageData16bit.push_back(a);
		}
	} else {
		std::cout << "Error opening image " << img_path << std::endl;
	}

	const auto img_tensor = img_to_tensor(imageData16bit, img_width, img_height);

	return img_tensor;
}

/// Function to score an image
std::tuple<int, float> score_image(std::string  img_path, fdeep::model blurModel, 
	fdeep::model coverageModel){

    // Load image as tensor
	// Twice, for the two networks, with different sizes
	
	//std::cout << "Load image for blur" << std::endl;
	fdeep::tensor img_tensor_blur = load_img(img_path, 512, 512);
	//fdeep::tensor img_tensor_blur = load_img(img_path, 1024, 1024);
	
	//std::cout << "Load image for coverage" << std::endl;
	fdeep::tensor img_tensor_cov = load_img(img_path, 128, 128);
	
	
	int score = 0;
	float cumulativeConfidence = 0.0;

	// Careful, the tags are in alphabetical order!
	// Blur:		["High", "Low", "Medium", "Sharp"]
	// Coverage:	["1Q", "3Q", "Full", "Half"]

	// Blur scoring
	
	int blurHigh_score		=	1;
	int blurMedium_score	= 	2;
	int blurLow_score		= 	3;
	int blurNone_score		= 	4;

	std::cout << "Scoring blur" << std::endl;
	const std::pair<long unsigned int, float> blurVerdict = 
			blurModel.predict_class_with_confidence({img_tensor_blur});
	std::cout << "Blur scored" << std::endl;

	long int blurCategory = blurVerdict.first;
	float blurConfidence = blurVerdict.second;
	
	cumulativeConfidence += blurConfidence;

	int blurScore;

	if		(blurCategory == 3)	{blurScore = blurNone_score;}		// Sharp
   	else if	(blurCategory == 2)	{blurScore = blurMedium_score;}		// Medium
   	else if	(blurCategory == 1)	{blurScore = blurLow_score;}		// Low
   	else						{blurScore = blurHigh_score;}		// High

	score += blurScore;

	// Coverage scoring

	int coverage1Q_score	=	1;
	int coverageHalf_score	= 	2;
	int coverage3Q_score	= 	3;
	int coverageFull_score	= 	4;	

	std::cout << "Scoring coverage" << std::endl;
	const std::pair<long unsigned int, float> coverageVerdict = 
			coverageModel.predict_class_with_confidence({img_tensor_cov});
	std::cout << "Coverage scored" << std::endl;

	long int coverageCategory = coverageVerdict.first;
	float coverageConfidence = coverageVerdict.second;

	cumulativeConfidence += coverageConfidence;

	int coverageScore;

	if		(coverageCategory == 3)	{coverageScore = coverageHalf_score;}	// Half
   	else if	(coverageCategory == 2)	{coverageScore = coverageFull_score;}	// Full
   	else if	(coverageCategory == 1)	{coverageScore = coverage3Q_score;}		// 3Q
   	else							{coverageScore = coverage1Q_score;}		// 1Q

	score += coverageScore;

	//std::cout << img_path << " | Blur score: " << blurScore << 
	//		"| Coverage score: " << coverageScore << std::endl;
	
	return{score, cumulativeConfidence};

}



int main() {

	// Loading models

	//// Blur model
    const fdeep::model blurModel = fdeep::load_model( "trainedModels/blurModel.json", true, fdeep::dev_null_logger);
    //const fdeep::model blurModel = fdeep::load_model( "trainedModels/blurModel1024.json", true, fdeep::dev_null_logger);

	//// Coverage model
    const fdeep::model coverageModel = fdeep::load_model("trainedModels/coverageModel.json", true, fdeep::dev_null_logger);

    // Initializing struct for storing results
	std::vector<Mixed> results;	


    // Opening image directory
	DIR *d;
	struct dirent *dir;

    //const char *foldername = "testImages512";
    const char *foldername = "testImages1024/";

	d = opendir(foldername);

	if (d == NULL) {
		std::cout << "Open dir failed " << std::endl;
		return -1;
	}

	// Iterating over files in folder (careful, there shouldn't be anything other filetype there)
	while ((dir = readdir(d)) != NULL) {

		if (dir->d_type == DT_REG) {

			std::string fileName = dir->d_name;

			std::string path = foldername + fileName; 

			// Scores every image and pushes the result, to be printed later

			auto [score, confidence] = score_image(path, blurModel, coverageModel);

			results.push_back({path, score, confidence});
		}
	}

	// Sort by score, untie by confidence

	struct sort_pred {
    	bool operator()(const Mixed& left, const Mixed& right) const {

			return std::tie(left.score, left.confidence) > 
        	std::tie(right.score, right.confidence);
    	}
	};

	std::sort(results.begin(), results.end(), sort_pred());

	// Iterate over the results and print
	std::vector<Mixed>::iterator it;

	
    // For debug: prints filename, scores and confidence.	
	for(it = results.begin(); it != results.end(); it++){

		std::cout << it->name << " score: " << std::to_string(it->score) 
		          << " conf: " << std::to_string(it->confidence) << std::endl;
		
		// Printing filenames in order.
		//std::cout << it -> name << std::endl;
		
	}
}
