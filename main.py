"""
This script loads the trained models for Blur, Coverage, and Image count,
plus the weights of the scores. It then reads images from the folder "testImages"
and returns an ordered list, from best to worse.
"""

import os
import keras.preprocessing

import numpy as np  # Only using numpy for argmax. Other way?
import tensorflow as tf

# Import scores from external file
import scores

# Folder with images to be tested #################

folderName = "testImages"

# Model loading ###################################

class trainedModel(object):

    def __init__(self, modelName, labelsList, imgDims):

            # Name of the trained model
            self.model = keras.models.load_model(f"trainedModels/{modelName}")
            #self.model = tf.keras.models.load_model(f"trainedModels/{modelName}")

            # List of categories the CNN has been trained on.
            self.labels = labelsList

            # Image dimensions that the CNN has trained on.
            # I hate to hardcode them like this, but I haven't found a way
            # to obtain this value from the trained models.
            self.imgDims = imgDims


# Labels must be in alphabetical order, because Keras says so.
blurLabels      =   ["High", "Low", "Medium", "Sharp"]
trainedBlur     =   trainedModel("blurModel", blurLabels, (128, 128))

coverageLabels  =   ["1Q", "3Q", "Full", "Half"]
#trainedCoverage =   trainedModel("coverageModel", coverageLabels, (100, 100))
trainedCoverage =   trainedModel("coverageModel", coverageLabels, (128, 128))

countLabels     =   ["Both", "D1", "D2", "None"]
trainedCount    =   trainedModel("objectCountModel", countLabels, (100, 100))

# Image scoring #################

def testImage(imageName, trainedModel):

    img = keras.preprocessing.image.load_img(imageName, 
            target_size = trainedModel.imgDims,
            grayscale = True)

    img_array = keras.preprocessing.image.img_to_array(img)

    # Rescaling
    img_array /= 255.0


    img_array = tf.expand_dims(img_array, 0)

    predictions = trainedModel.model.predict(img_array)
    score = tf.nn.softmax(predictions[0])   # Is softmax what I want?
                                            # Also, am I using softmax twice?

    # Try to replace np max and argmax with pure python

    #prediction = trainedModel.labels[np.argmax(score)]
    # The following nonsense allows me to avoid using np.argmax
    prediction = trainedModel.labels[list(score).index(max(list(score)))]

    # Two things about confidence: first, maybe it's not possible to find the
    # max value of a tensor without np. Second, tensorflow uses numpy anyway!!
    confidence = 100 * np.max(score)


    return prediction, confidence


path = os.path.abspath(os.getcwd() + "/" + folderName + "/")

def scoreImage(img):

    score = 0
    cumulativeConfidence = 0

    blurPrediction      =   testImage(img, trainedBlur)
    coveragePrediction  =   testImage(img, trainedCoverage)
    countPrediction     =   testImage(img, trainedCount)

    # Blur

    if blurPrediction[0] == "High":
        score += scores.blurHigh
    elif blurPrediction[0] == "Medium":
        score += scores.blurMedium
    elif blurPrediction[0]  == "Low":
        score += scores.blurLow
    else:
        score += scores.blurNone

    cumulativeConfidence += blurPrediction[1]

    # Coverage

    if coveragePrediction[0]  == "1Q":
        score += scores.coverage1Q
    elif coveragePrediction[0]  == "Half":
        score += scores.coverageHalf 
    elif coveragePrediction[0]  == "3Q":
        score += scores.coverage3Q
    else:
        score += scores.coverageFull

    cumulativeConfidence += coveragePrediction[1]

    # Object count

    if countPrediction[0] == "D2" or countPrediction[0]  == "Both":
        D2Status = "Dimorphos covered"
        score += scores.D2visible 
    else:
        D2Status = ""
        score += scores.D2notVisible

    # D2 detection is a sidequest, so leaving this commented out for now.
    #cumulativeConfidence += countPrediction[1]

    predictions = f"{blurPrediction[0]}, {coveragePrediction[0]}, {D2Status}"

    return score, predictions, cumulativeConfidence
    

images = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

verdict = []

for image in images:
    score, predictions, cumulativeConfidence = scoreImage(path + "/" + image)
    verdict.append((image, predictions, score, int(cumulativeConfidence)))



# Sorting by score first and confidence second.
verdict = sorted(verdict, key=lambda tup: (tup[2], tup[3]), reverse = True)

for line in verdict:
    print(line)
