TBD

* Converting to binary:

'''
for filename in *; do convert -depth 16 -size 1024x1024 $filename gray:$filename.img; done
'''

* G++ compiler

'''
g++ -O3 -o main main.cpp -lpthread -lX11 (+ verbose if needed)
'''
