from pathlib import Path

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout, BatchNormalization, Activation

from models.model_generator import ModelGenerator

# Variables

p = Path(__file__).parents[3]

coverage_num_classes         =   4          # Full, 3Q, Half, 1Q

coverage_batch_size          =   16         # Reduce if "Allocation exceeds free system memory"

coverage_img_dims            =   (128, 128) # Images will be reduced to this before the NN

coverage_brightness_range    =   (0.7, 2.0)


# Layer design


coverageLayers = Sequential()

coverageLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same", input_shape = (coverage_img_dims[0], coverage_img_dims[1], 1)))
coverageLayers.add(Activation("relu"))
coverageLayers.add(MaxPooling2D(pool_size=(2, 2)))

coverageLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same"))
coverageLayers.add(Activation("relu"))
coverageLayers.add(MaxPooling2D(pool_size=(2, 2)))

coverageLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same"))
coverageLayers.add(Activation("relu"))
coverageLayers.add(MaxPooling2D(pool_size=(2, 2)))

coverageLayers.add(Flatten())
coverageLayers.add(Dense(8, activation='relu')) # The 16 seems to be vital now. Weird.
coverageLayers.add(Dense(coverage_num_classes, activation='softmax'))


def model(folder: str) -> ModelGenerator:
    return ModelGenerator (
        name                =   "coverageModel",
        path_train          =   str(p) + folder + "coverageImages/Training/",
        path_test           =   str(p) + folder + "coverageImages/Testing/",
        batch_size          =   coverage_batch_size,
        img_dims            =   coverage_img_dims, 
        brightness_range    =   coverage_brightness_range, 
        layers              =   coverageLayers
    )
