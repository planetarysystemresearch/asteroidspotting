import os

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout, BatchNormalization, Activation
from keras.preprocessing.image import ImageDataGenerator

class ModelGenerator(object):
    """
    This objects will correspond to a specific neural network model,
    with its own specialization, parameters, and training images.
    Returns model and model name, maybe?
    No, it won't return the model name, it will be self variable.
    """

    def __init__(self, name, path_train, path_test, batch_size, img_dims, brightness_range, layers):

        """Constructor"""
        self.name       =   name
        self.path_train =   path_train
        self.path_test  =   path_test   # Needed here? Should it be outside the class?
        self.img_dims   =   img_dims
        self.img_width  =   self.img_dims[0]
        self.img_height =   self.img_dims[1]
        self.batch_size =   batch_size
        self.brightness_range = brightness_range
        self.layers     =   layers

        # Datagen randomises the images.
        self.datagen = ImageDataGenerator(
            rescale             =   1 / 255.0,
            rotation_range      =   20,
            horizontal_flip     =   True,
            vertical_flip       =   True,
            brightness_range    =   self.brightness_range,
            validation_split    =   0.20,
        )

        self.train_generator = self.datagen.flow_from_directory(
            directory   =   self.path_train,
            target_size =   self.img_dims,
            color_mode  =   "grayscale",
            batch_size  =   self.batch_size,
            class_mode  =   "categorical",
            subset      =   "training",
            shuffle     =   True,
            seed        =   42
        )

        self.valid_generator = self.datagen.flow_from_directory(
            directory   =   self.path_train, 
            target_size =   self.img_dims,
            color_mode  =   "grayscale",
            batch_size  =   self.batch_size,
            class_mode  =   "categorical",
            subset      =   "validation",
            shuffle     =   "true",
            seed        =   42,
        )
