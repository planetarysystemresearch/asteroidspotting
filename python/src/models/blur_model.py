from pathlib import Path

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout, BatchNormalization, Activation

from models.model_generator import ModelGenerator

# Variables

p = Path(__file__).parents[3]

blur_num_classes         =   4      # Sharp, Low, Medium, and High.

blur_batch_size          =   16     # Reduce this if "Allocation exceeds free system memory"

blur_img_dims            =   (512, 512)

blur_brightness_range    =   (0.5, 1.5)


# Layer design

blurLayers = Sequential()

blurLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same", input_shape = (blur_img_dims[0], blur_img_dims[1], 1)))
blurLayers.add(Activation("relu"))
blurLayers.add(MaxPooling2D(pool_size=(2, 2)))


blurLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same"))
blurLayers.add(Activation("relu"))
blurLayers.add(MaxPooling2D(pool_size=(2, 2)))

blurLayers.add(Conv2D(16, kernel_size=(3,3), padding = "same"))
blurLayers.add(Activation("relu"))
blurLayers.add(MaxPooling2D(pool_size=(2, 2)))

blurLayers.add(Flatten())

blurLayers.add(Dense(16, activation='relu'))
blurLayers.add(Dense(blur_num_classes, activation='softmax'))


def model(folder: str) -> ModelGenerator:
    return ModelGenerator (
        name                =   "blurModel",
        path_train          =   str(p) + folder + "blurImages/Training/",
        path_test           =   str(p) + folder + "blurImages/Testing/",
        batch_size          =   blur_batch_size,
        img_dims            =   blur_img_dims, 
        brightness_range    =   blur_brightness_range, 
        layers              =   blurLayers
    )
