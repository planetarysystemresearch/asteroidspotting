import os
import cv2
import random
import numpy as np
from matplotlib import pyplot as plt

mode = "Folder"      # Options: "Image" and "Folder"
print(f"Mode: {mode}")

folderName = "imageBank-New"
sharpFolder = "Original"
blurredFolder = "Blurred"

# This is a bit of a disaster, will fail if not called from its folder.
# Also what about windows, will the following line work at all?

os.chdir("..")  # Ascend one level
sharpPath = os.getcwd() + "/" + folderName + "/" + sharpFolder
blurPath = os.getcwd() + "/" + folderName + "/" + blurredFolder

#-------------------------------------------------------------------------------------

def blurUniform(img, blurAmount):

    return cv2.blur(img,(blurAmount,blurAmount))

def blurMotionOld(img, blurAmount):
    """
    Only horizontal for now, need more
    """

    kernel_motion_blur = np.zeros((blurAmount, blurAmount))
    # Horizontal
    #kernel_motion_blur[int((blurAmount-1)/2), :] = np.ones(blurAmount)
    # Vertical?
    kernel_motion_blur[:, int((blurAmount-1)/2)] = np.ones(blurAmount)
    kernel_motion_blur = kernel_motion_blur / blurAmount

    return cv2.filter2D(img, -1, kernel_motion_blur)

def blurMotion(img, blurAmount, angle):
    """
    Another stackoverflow answer, but for any angle
    """

    # All this is the kernel, right?
    k = np.zeros((blurAmount, blurAmount), dtype=np.float32)
    k[ (blurAmount-1)// 2 , :] = np.ones(blurAmount, dtype=np.float32)
    k = cv2.warpAffine(k, cv2.getRotationMatrix2D( (blurAmount / 2 -0.5 , blurAmount / 2 -0.5 ) , angle, 1.0), (blurAmount, blurAmount) )  
    k = k * ( 1.0 / np.sum(k) )        

    return cv2.filter2D(img, -1, k) 



#-------------------------------------------------------------------------------------

if mode == "Image":

    blurAmount = 30 # This should correspond to the number of pixels displaced, does it?

    img = cv2.imread(imgName, 0)  # The zero turns it into grayscale
    rows,cols = img.shape   # Needed?

    result = blurMotion(img, blurAmount, 135)
    #result = blurMotion(img, blurAmount)
    
    cv2.imwrite("image_blurred" + "-b" + str(blurAmount) + ".png", result)
    #cv2.imwrite("image_blurred.png", result)

    """
    # Plots
    plt.subplot(121),plt.imshow(img, cmap = "gray"),plt.title("Original")
    plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(result, cmap = "gray"),plt.title("Blurred")
    plt.xticks([]), plt.yticks([])
    plt.show()
    """

elif mode == "Folder":


    originalImages = [f for f in os.listdir(sharpPath)\
            if os.path.isfile(os.path.join(sharpPath, f))]

    os.chdir(blurPath)

    blurAmounts = [5, 15, 50]   # Aprox. logarithmic
    blurCategories = ["Low", "Medium", "High"]
    
    for imageName in originalImages:
        
        # Read original image
        #img = cv2.imread(sharpPath + "/" + imageName, 1)

        # Does this solve the 8-bit issue?
        # Seems like it does.
        img = cv2.imread(sharpPath + "/" + imageName, -1)

        # Remove suffix
        imageName = imageName.removesuffix(".png")

        for index, blur in enumerate(blurAmounts):

            angle = random.randint(0, 359)

            try:
                randomBlur = random.randint(blurAmounts[index - 1], blur)
            except:
                # If index = 0, the above will fail.
                randomBlur = random.randint(1, blur) 

            #blurredImg = blurMotion(img, blur, angle)
            blurredImg = blurMotion(img, randomBlur, angle)

            print(f"Blurrying {imageName} with strenght of {randomBlur} and {angle} degrees angle.")
            cv2.imwrite(blurPath + "/" + blurCategories[index] + "/" + imageName + ".png", blurredImg)
