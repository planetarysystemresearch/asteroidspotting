import numpy as np
import random
import time
import math

# User input

def inputNumber(message):
  while True:
    try:
        variable = int(input(message))       
        
        if variable < 1:
            print("The number must be above 0.")
            #break
            continue

    except ValueError:
       print("Not an integer! Try again.")
       continue
    else:
       return variable 
       break 

imageAmount         =   inputNumber("Choose number of images: ")
minCameraDistance   =   inputNumber("Choose minimum camera distance (km): ")
maxCameraDistance   =   inputNumber("Choose maximum camera distance (km): ")

## CAMERA COORDINATES

# Array of random coordinates between 0 and 1.
cameraCoordinates = np.random.rand(imageAmount,3)

# Normalize the module of all the vectors (i.e.: rows) to 1.

def length(row):
    return( math.sqrt( row[0]**2 + row[1]**2 + row[2]**2 ) )

lengths = np.apply_along_axis(length, 1, cameraCoordinates)

normalized_array = cameraCoordinates  / lengths[:, np.newaxis]

# Array with random distances...
randomDistances = np.random.rand(imageAmount, 1)

# ...remapped to the desired range.
randomDistances = np.interp(randomDistances,
            (randomDistances.min(), randomDistances.max()),
            (minCameraDistance, maxCameraDistance))

cameraCoordinates = normalized_array * randomDistances

# Array with random signs

signs = np.array([(-1)**random.randint(0,1) for i in range(imageAmount*3)])
signs = signs.reshape(imageAmount, 3)

# Final array
cameraCoordinates = np.multiply(cameraCoordinates, signs)

## SUN COORDINATES

sunCoordinates = np.random.rand(imageAmount,3)

minSunCoord = 1  # Greater than zero to prevent problems
maxSunCoord = 3  # Max distance doesn't matter that much with sunlamps

# This might look confusing, think of other solution.

# Remap values
sunCoordinates = np.interp(sunCoordinates, 
        (sunCoordinates.min(), sunCoordinates.max()),       # Remap from
        (minSunCoord, maxSunCoord))                         # To

# Randomize signs

signs = np.array([(-1)**random.randint(0,1) for i in range(imageAmount*3)])
signs = signs.reshape(imageAmount, 3)

sunCoordinates = np.multiply(sunCoordinates, signs)


## SUN IRRADIATION

# Should the irradiation be randomized? I'll keep it constant for now

irradiation = np.full((imageAmount, 1), 250) 

## SYSTEM ROTATION

systemRotation = np.random.rand(imageAmount,1)

# Remap values
systemRotation = np.interp(systemRotation, 
        (systemRotation.min(), systemRotation.max()),       # Remap from
        (0, 360))                                           # To


##########################################

result = arr = np.concatenate(
        (cameraCoordinates, sunCoordinates, irradiation, systemRotation),
        axis=1)

result = np.round(result, 3)    # The effect is gone in the .txt

header = "camera_x\tcamera_y\tcamera_z\tsun_x\tsun_y\tsun_z\tsun_irr\trot" 

#print(header)
#print(result)

filename = (f"../datafiles/{imageAmount}-images"
        f"-camera({minCameraDistance},{maxCameraDistance})"
        #f"-sun({minSunCoord},{maxSunCoord})"
        "-" + str(time.time()) + ".txt")

np.savetxt(filename, result, fmt="%1.3f", delimiter = "\t", header = header, comments = "")
# fmt="%1.3f" -> saves with 3 decimal points.
# comments = "" removes the hash mark at the start of the line

print(f"Saved file '{filename}'")
