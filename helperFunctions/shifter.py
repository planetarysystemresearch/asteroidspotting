import os
import cv2
import random
import numpy as np

mode = "Folder"  # "Image" or "Folder"

imgName         =   "example.png"
folderName      =   "imageBank-New" 
originalFolder  =   "Original"
shiftedFolder   =   "Shifted"

os.chdir("..")  # Ascend one level
originalPath    =   os.getcwd() + "/" + folderName + "/" + originalFolder
shiftedPath     =   os.getcwd() + "/" + folderName + "/" + shiftedFolder

maxShift        =   350  

def shiftImage(img, shiftX, shiftY):
    """
    image: imported with cv2.
    shiftX and shiftY: int, corresponding to a pixel amount.
    """

    # Horizontal shift

    if shiftX > 0:

        img = np.pad(img, ( (0,0),(shiftX,0)), mode = "constant")[:,:-shiftX]

    elif shiftX < 0:

        img = np.pad(img, ( (0,0),(0,-shiftX)), mode = "constant")[:,-shiftX:]

    # Vertical shift
    if shiftY > 0:
        # Moves down

        img = np.pad(img, ( (shiftY,0),(0,0)), mode = "constant")[:-shiftY,:]

    elif shiftY < 0:
        # Moves up

        img = np.pad(img, ( (0,-shiftY),(0,0)), mode = "constant")[-shiftY:,:]

    return img

if mode == "Image":

    # Read image
    #img = cv2.imread(imgName, 0)

    # Changing to -1 so that the 16 bit image is respected.
    img = cv2.imread(imgName, -1)

    # Shift it

    shiftX = 250     # pixels
    shiftY = -250     # pixels


    shiftX = random.randint(-maxShift, maxShift)
    shiftY = random.randint(-maxShift, maxShift)
    totalShift = int(np.sqrt(shiftX**2 + shiftY**2))

    imgShifted = shiftImage(img, shiftX, shiftY) #this fails, but worked a moment ago!

    # Save it

    imgName = imgName.removesuffix(".png")

    # I add the pattern -sXX, XX being the approximate displacement
    #cv2.imwrite(imgName + "shifted" + "-s" + str(int(np.sqrt(shiftX**2 + shiftY**2)))\
    cv2.imwrite(imgName + "shifted" + "-s" + str(totalShift)\
            + ".png", imgShifted)




elif mode == "Folder":

    """
    folderPath = os.getcwd() + "/" + folderName
    newFolderPath = folderPath.replace("Centered", "Offset")
    # Note: if the folder doesn't exist, it will not work.

    os.chdir(folderPath)
    filenames = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]
    """

    originalImages = [f for f in os.listdir(originalPath)\
            if os.path.isfile(os.path.join(originalPath, f))]

    os.chdir(shiftedPath)

    # It would be great to automatically classify the images here as well, but
    # the coverage will have to be done by hand, sadly.

    for imageName in originalImages:

        shiftX = random.randint(-maxShift, maxShift)
        shiftY = random.randint(-maxShift, maxShift)
        totalShift = int(np.sqrt(shiftX**2 + shiftY**2))

        # Read image
        #img = cv2.imread(originalPath + "/" + imageName, 0)
        # Changing to -1 so that the 16 bit image is respected.
        img = cv2.imread(originalPath + "/" + imageName, -1)

        print(f"Shifting {imageName}, strength {totalShift}.")

        imgShifted = shiftImage(img, shiftX, shiftY) 

        imageName = imageName.removesuffix(".png")

        #cv2.imwrite(shiftedPath + "/" + imageName + "-s" +
        #        str(totalShift) + ".png", imgShifted)

        cv2.imwrite(shiftedPath + "/" + imageName + ".png", imgShifted)
