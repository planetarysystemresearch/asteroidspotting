# Blur

blurHigh        =   -5
blurMedium      =   -3
blurLow         =   2
blurNone        =   5

# Coverage

coverage1Q      =   -5
coverageHalf    =   -3
coverage3Q      =   2
coverageFull    =   5

# Object count

D2visible       =   0
D2notVisible    =   0
