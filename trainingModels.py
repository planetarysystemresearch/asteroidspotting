import os
import re
import time
import keras # from keras import preprocessing?
import pathlib
import numpy as np
import tensorflow as tf
from tabulate import tabulate

from sys import exit

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout
from keras.preprocessing.image import ImageDataGenerator

from tensorflow.keras.callbacks import TensorBoard

# Import models from the other file.
from models import blurModel, coverageModel

modelSelection = input("Select model (1: Blur, 2: Coverage)")

if modelSelection   == "1":
    uncompiledModel = blurModel
elif modelSelection == "2":
    uncompiledModel = coverageModel
else:
    print("Incorrect selection. Exiting.")
    exit()

retrain = input("Should the model be retrained? (Y/N)")

if retrain.lower() == "y":
    
    epochs = int(input("Choose number of epochs for training: "))

    modelName = (f"{uncompiledModel.name}"
    #f"-layerSize{uncompiledModel.layer_size}"
    f"-{uncompiledModel.img_height}x{uncompiledModel.img_width}"
    f"-brightnessRange{str(uncompiledModel.brightness_range)}"
    f"-{int(time.time())}")

    tensorboard = TensorBoard(log_dir = f"logs/{modelName}")

    def trainModel(uncompiledModel):

        model = uncompiledModel.layers

        model.compile(
            loss        =   "categorical_crossentropy",
            optimizer   =   "adam",
            metrics     =   ["accuracy"]
            )

        train_generator     =   uncompiledModel.train_generator
        valid_generator     =   uncompiledModel.valid_generator

        model.fit_generator(
                train_generator,
                validation_data = train_generator,
                steps_per_epoch = train_generator.n//train_generator.batch_size,
                validation_steps = valid_generator.n//valid_generator.batch_size,
                epochs = epochs,
                callbacks = [tensorboard]
                )

        return model
    

    model = trainModel(uncompiledModel)
    model.summary() 

    print("Saving normally.")
    model.save(f"trainedModels/{uncompiledModel.name}")
    
    #Saving this other way for using frugally-deep
    print("Saving in h5 format.")
    model.save(f"trainedModels/{uncompiledModel.name}h5/model.h5", include_optimizer = False)
    

else:

    model = keras.models.load_model(f"trainedModels/{uncompiledModel.name}")

#############################################################################3

# Predict on new data

label_dict = (uncompiledModel.train_generator.class_indices)
labels = list(label_dict.keys())

def testImage(imageName, model):

    img = tf.keras.utils.load_img(imageName, 
            target_size = (uncompiledModel.img_width, uncompiledModel.img_height), 
            interpolation = "bilinear",
            grayscale = True)

    #img_array = keras.preprocessing.image.img_to_array(img)
    img_array = tf.keras.utils.img_to_array(img)

    # Rescaling
    img_array /= 255.0  # Now it's a tiny bit better!!!


    img_array = tf.expand_dims(img_array, 0) # Create a batch

    predictions = model.predict(img_array)
    score = tf.nn.softmax(predictions[0])   # Is softmax what I want?
    # Am I using softmax twice?

    prediction = labels[np.argmax(score)]
    confidence = 100 * np.max(score)

    return prediction, confidence

def generateResultsTable(folderPath, model):

    
    imgNames = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]
    # This fails if there's something in the folder that is not an image!
    # So I filter everything except png and jpg
    imgNames = list(filter(lambda x:x.endswith((".png", ".jpg")), imgNames))
        
    # TEST: Read metadata

    with open(folderPath + "metadata.txt") as f:
        metadata = f.readlines()
        
    #print(metadata)

    table = []
    successes = closecalls = failures = 0

    for imgName in sorted(imgNames):

        row = []

        row.append(imgName)

        # This is inefficient, improve it at some point.
        for line in metadata:
            line = line.split()
            #print(line)
            if line[0] == imgName:
                expected = line[1]
                break

        row.append(expected)

        prediction, confidence = testImage(folderPath + imgName, model)
        row.append(prediction) 

        #if expected.capitalize() == prediction:
        if expected == prediction:
            row.append("✔")
            successes += 1
        else:

            if uncompiledModel.name == "blurModel":

                if expected.capitalize() != "Sharp" and prediction != "Sharp":
                    # Detected blur correctly, failed with the amount.
                    row.append("~")
                    closecalls += 1
                else:
                    row.append("x")
                    failures += 1
            else:

                row.append("x")
                failures += 1

        row.append(f"{confidence:.2f}%")

        table.append(row)

    headers = ["Image name", "Expected", "Prediction", "", "Confidence"]

    total = len(imgNames)
    successesP = (successes / total) * 100
    closecallsP = (closecalls / total) * 100
    failuresP = (failures / total) * 100

    return tabulate(table, headers), (successesP, closecallsP, failuresP)

#############################################################################3

# Pretty output

realImagesTable, realImagesStatistics = generateResultsTable(
        uncompiledModel.path_test + "Real/", model)
        #blurModel.path_test + "Real/", model)

syntheticImagesTable, syntheticImagesStatistics = generateResultsTable(
        uncompiledModel.path_test + "Synthetic/", model)
        #blurModel.path_test + "Synthetic/", model)

print(f"\n{model.name}")

print("\nReal images:")
print(realImagesTable)
print(f"\n{realImagesStatistics[0]:.2f}% success, {realImagesStatistics[1]:.2f}% close-calls, {realImagesStatistics[2]:.2f}% failure.")

print("\nSynthetic images:")
print(syntheticImagesTable)
print(f"\n{syntheticImagesStatistics[0]:.2f}% success, {syntheticImagesStatistics[1]:.2f}% close-calls, {syntheticImagesStatistics[2]:.2f}% failure.")
