import os

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout, BatchNormalization, Activation
from keras.preprocessing.image import ImageDataGenerator

class ModelGenerator(object):
    """
    This objects will correspond to a specific neural network model,
    with its own specialization, parameters, and training images.
    Returns model and model name, maybe?
    No, it won't return the model name, it will be self variable.
    """

    def __init__(self, name, path_train, path_test, batch_size, img_dims, brightness_range, layers):

        """Constructor"""
        self.name       =   name
        self.path_train =   path_train
        self.path_test  =   path_test   # Needed here? Should it be outside the class?
        self.img_dims   =   img_dims
        self.img_width  =   self.img_dims[0]
        self.img_height =   self.img_dims[1]
        self.batch_size =   batch_size
        self.brightness_range = brightness_range
        self.layers     =   layers

        # Datagen randomises the images.
        self.datagen = ImageDataGenerator(
            rescale             =   1 / 255.0,
            rotation_range      =   20,
            horizontal_flip     =   True,
            vertical_flip       =   True,
            brightness_range    =   self.brightness_range,
            validation_split    =   0.20,
        )

        self.train_generator = self.datagen.flow_from_directory(
            directory   =   self.path_train,
            target_size =   self.img_dims,
            color_mode  =   "grayscale",
            batch_size  =   self.batch_size,
            class_mode  =   "categorical",
            subset      =   "training",
            shuffle     =   True,
            seed        =   42
        )

        self.valid_generator = self.datagen.flow_from_directory(
            directory   =   self.path_train, 
            target_size =   self.img_dims,
            color_mode  =   "grayscale",
            batch_size  =   self.batch_size,
            class_mode  =   "categorical",
            subset      =   "validation",
            shuffle     =   "true",
            seed        =   42,
        )



#################################################################################

## Blur CNN

# Variables

blur_images_folder  =   "/March2023Training/imageDatasets/blurImages"

blur_pathTrain      =   os.getcwd() + blur_images_folder + "/Training/"
blur_pathTest       =   os.getcwd() + blur_images_folder + "/Testing/"

blur_num_classes         =   4      # Sharp, Low, Medium, and High.

blur_batch_size          =   16     # Reduce this if "Allocation exceeds free system memory"

blur_img_dims            =   (512, 512)

blur_brightness_range    =   (0.5, 1.5)


# Layer design

blurLayers = Sequential()

blurLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same", input_shape = (blur_img_dims[0], blur_img_dims[1], 1)))
blurLayers.add(Activation("relu"))
blurLayers.add(MaxPooling2D(pool_size=(2, 2)))


blurLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same"))
blurLayers.add(Activation("relu"))
blurLayers.add(MaxPooling2D(pool_size=(2, 2)))

blurLayers.add(Conv2D(16, kernel_size=(3,3), padding = "same"))
blurLayers.add(Activation("relu"))
blurLayers.add(MaxPooling2D(pool_size=(2, 2)))

blurLayers.add(Flatten())

blurLayers.add(Dense(16, activation='relu'))
blurLayers.add(Dense(blur_num_classes, activation='softmax'))

# Result

blurModel = ModelGenerator(
        name                =   "blurModel",
        path_train          =   blur_pathTrain,
        path_test           =   blur_pathTest, 
        batch_size          =   blur_batch_size,
        img_dims            =   blur_img_dims, 
        brightness_range    =   blur_brightness_range, 
        layers              =   blurLayers
        )
        
#################################################################################

## Coverage CNN

# Variables

coverage_images_folder  =   "/March2023Training/imageDatasets/coverageImages"

coverage_pathTrain      =   os.getcwd() + coverage_images_folder + "/Training/"
coverage_pathTest       =   os.getcwd() + coverage_images_folder + "/Testing/"


coverage_num_classes         =   4 # Full, 3Q, Half, 1Q

coverage_batch_size          =   16  # Reduce if "Allocation exceeds free system memory"

coverage_img_dims            =   (128, 128) # Images will be reduced to this before the NN

#coverage_brightness_range    =   (1.5, 1.5)
coverage_brightness_range    =   (0.7, 2.0)


# Layer design


coverageLayers = Sequential()

coverageLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same", input_shape = (coverage_img_dims[0], coverage_img_dims[1], 1)))
coverageLayers.add(Activation("relu"))
coverageLayers.add(MaxPooling2D(pool_size=(2, 2)))

coverageLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same"))
coverageLayers.add(Activation("relu"))
coverageLayers.add(MaxPooling2D(pool_size=(2, 2)))

coverageLayers.add(Conv2D(32, kernel_size=(3,3), padding = "same"))
coverageLayers.add(Activation("relu"))
coverageLayers.add(MaxPooling2D(pool_size=(2, 2)))

coverageLayers.add(Flatten())
coverageLayers.add(Dense(8, activation='relu')) # The 16 seems to be vital now. Weird.
coverageLayers.add(Dense(coverage_num_classes, activation='softmax'))

# Result

coverageModel = ModelGenerator(
        name                =   "coverageModel",
        path_train          =   coverage_pathTrain,
        path_test           =   coverage_pathTest, 
        batch_size          =   coverage_batch_size,
        img_dims            =   coverage_img_dims, 
        brightness_range    =   coverage_brightness_range, 
        layers              =   coverageLayers
        )


#################################################################################

## Object count CNN
# Detects the amount of FULLY COVERED asteroids on sight.

# Variables


#count_pathTrain    =   os.getcwd() + "/imageDatasets/objectCountImages/Training/"
#count_pathTest     =   os.getcwd() + "/imageDatasets/objectCountImages/Testing/"
#
## Re-used variable name from above. Does it matter in this situation?
#
#count_num_classes         =   4 # D1, D2, Both, None
#
#count_layer_size          =   32
#
#count_batch_size          =   64  # Reduce if "Allocation exceeds free system memory"
#
#count_img_dims            =   (100, 100)
#
##count_brightness_range    =   (1.5, 1.5)
#count_brightness_range    =   (0.7, 2.0)
#
#
## Layer design
## To try: Dropouts, 
#
#extra_conv  =   2   # Number of convolution layers other than the first.
#
#countLayers = Sequential()
#
##countLayers.add(Conv2D(count_layer_size, kernel_size=(3,3), padding = "same", activation='relu'))
#
## Trying to re-add input_shape, because frugally-deep is protesting
## It worked!
#countLayers.add(Conv2D(count_layer_size, kernel_size=(3,3), padding = "same", activation='relu', input_shape = (count_img_dims[0], count_img_dims[1], 1)))
#
#countLayers.add(MaxPooling2D(pool_size=(2, 2)))
#
#for i in range(extra_conv):
#    countLayers.add(Conv2D(count_layer_size, 3, padding = "same", activation = "relu"))
#    countLayers.add(MaxPooling2D())
#
#countLayers.add(Flatten())
#countLayers.add(Dense(16, activation='relu')) # The 16 seems to be vital now. Weird.
#countLayers.add(Dense(count_num_classes, activation='softmax'))
#
## Result
#
#objectCountModel = ModelGenerator(
#        name                =   "objectCountModel",
#        path_train          =   count_pathTrain,
#        path_test           =   count_pathTest, 
#        layer_size          =   count_layer_size,
#        batch_size          =   count_batch_size,
#        img_dims            =   count_img_dims, 
#        brightness_range    =   count_brightness_range, 
#        layers              =   countLayers
#        )














